# fengyong-admin 后台管理系统

## 启动项目命令

```
npm run serve
```

## 项目打包

```
npm run build
```

## 项目所用框架

> vue2.\* + vue-router + vuex + elementui + axios

- 1、安装项目所需依赖 element-ui，axios，reset-css (normalize.css)
- 2、main.js 引入依赖
- 3、初始化默认样式

## 注意事项

- 项目依赖（element-ui）如果使用的组件比较少，可以按需引入，减小打包体积，提升性能

## git commit 信息规范

```
feat: 新功能、新特性
fix: 修改 bug
perf: 更改代码，以提高性能（在不影响代码内部行为的前提下，对程序性能进行优化）
refactor: 代码重构（重构，在不影响代码内部行为、功能下的代码修改）
docs: 文档修改
style: 代码格式修改, 注意不是 css 修改（例如分号修改）
test: 测试用例新增、修改
build: 影响项目构建或依赖项修改
revert: 恢复上一次提交
ci: 持续集成相关文件修改
chore: 其他修改（不在上述类型中的修改）
release: 发布新版本
workflow: 工作流相关文件修改
```

## 项目功能点

### 登录

    * 1、表单校验
    * 2、跨域问题处理
    * 3、ajax传参【post】 application/json,application/x-www-form-urlencoded(把data转化为querystring（查询字符串）),application/form-data
    * 4、axios封装
    * 5、axios拦截器
    * 6、菜单递归组件生成
    * 7、面包屑二次封装
    * 8、登陆状态失效，路由拦截
    * 9、页面tab view 路由
    * 10、vue-fullscreen 全屏效果
    * 11、富文本编辑器，html，markdown,ace,tinymce,quilleditor
