module.exports = {
  lintOnSave: false,
  publicPath: process.env.NODE_ENV === "production" ? "" : "/",
  devServer: {
    port: 3000,
    // proxy: "http://test.raz-kid.cn",
    proxy: {
      "/api": {
        target: "http://admin.raz-kid.cn",
      },
      "/h5": {
        target: "https://fengyong-api.onrender.com",
        pathRewrite: {
          "^/h5": "",
        },
      },
      "/pc": {
        target: "http://192.168.1.42:4000",
        pathRewrite: {
          "^/pc": "",
        },
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/common/sass/var.scss";`,
      },
    },
  },
};

// const { defineConfig } = require("@vue/cli-service");
// module.exports = defineConfig({
//   transpileDependencies: true,

// });
