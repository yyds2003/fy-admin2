/*
 * @Author: huirui
 * @Date: 2022-10-Tu 03:23:09
 * @Last Modified by:   huirui
 * @Last Modified time: 2022-10-Tu 03:23:09
 */

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {},
});
