import http from "./index";
//登录
export const login = async function (data, loading) {
  const res = await http.call(
    this,
    "/api/manage/user/login.do",
    "POST",
    data,
    {},
    loading
  );
  return res;
};
// 首页基础数据信息接口
// GET /manage/statistic/base_count.do
// 接口ID：41905392
export const getIndex = async function () {
  const res = await http("/api/manage/statistic/base_count.do");
  return res.data;
};
