/*
 * @Author: huirui
 * @Date: 2022-10-Tu 03:19:56
 * @Last Modified by:   huirui
 * @Last Modified time: 2022-10-Tu 03:19:56
 */

import axios from "axios";
import { Message } from "element-ui";
import qs from "qs";
import router from "@/router";
// axios.defaults.baseURL = "";
// axios.defaults.headers.post["Content-Type"] =
//123
//   "application/x-www-form-urlencoded";
let instance = axios.create({
  baseURL: "",
  //设置超时时间
  timeout: 15000,
  headers: {
    post: {
      "Content-type": "application/x-www-form-urlencoded",
    },
  },
});
let instanceOne = axios.create({
  timeout: 5000,
  baseURL: "/h5/",
});
let formData = axios.create({
  baseURL: "",
  //设置超时时间
  timeout: 15000,
});
function post(url, data = {}) {
  return instance
    .post(url, new URLSearchParams(qs.stringify(data)))
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        if (res.data.status === 0) {
          return res.data;
        } else {
          Message.err(res.data.msg);
          return Promise.reject(res.data.msg);
        }
      } else {
        Message.err(res.statusText);
        return Promise.reject(res.statusText);
      }
    })
    .catch((err) => {
      Promise.reject(err);
    });
}

function get(url, params = {}) {
  return instance
    .get(url, {
      params,
    })
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        if (res.data.status === 0) {
          return res.data;
        } else if (res.data.status === 10) {
          Message.error(res.data.msg);
          router.replace({
            name: "login",
            query: {
              redirect: router.currentRoute.fullPath,
            },
          });
        } else {
          Promise.reject(res.data.msg);
        }
      } else {
        Promise.reject(res.statusText);
      }
    })
    .catch((err) => {
      Promise.reject(err);
    });
}

function getTwo(url, params) {
  return instanceOne
    .get(url, {
      params,
    })
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        if (res.data.success) {
          return res.data;
        } else {
          Promise.reject(res.data.msg);
        }
      } else {
        Promise.reject(res.statusText);
      }
    });
}
function upload(url, data = {}, config) {
  const obj = new FormData();
  Object.keys(data).map((item) => {
    obj.append(item, data[item]);
  });
  return formData
    .post(url, obj, config)
    .then((res) => {
      if (res.status >= 200 && res.status < 300) {
        if (res.data.code === 0) {
          return res.data;
        } else {
          Message.err(res.data.msg);
          return Promise.reject(res.data.msg);
        }
      } else {
        Message.err(res.statusText);
        return Promise.reject(res.statusText);
      }
    })
    .catch((err) => {
      Promise.reject(err);
    });
}
export { post, get, getTwo, upload };
