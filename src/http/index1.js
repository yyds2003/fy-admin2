import axios from "axios";
import { Message } from "element-ui";
import router from "@/router";
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";
// axios.defaults.baseURL = "http://test.raz-kid.cn"; //开发
// axios.defaults.baseURL = "http://test.raz-kid.cn"; //测试
// axios.defaults.baseURL = "http://test.raz-kid.cn";//线上
const instance = axios.create({
  baseURL: "",
  //   baseURL: "http://test.raz-kid.cn",
  //   baseURL: "http://test.raz-kid.cn",
});
const http = function (url, method = "GET", data = {}, params = {}, loading) {
  let _data = {};
  if (
    Object.prototype.toString.call(data) === "[object Object]" &&
    Object.keys(data).length > 0
  ) {
    _data = new URLSearchParams();
    Object.keys(data).map((key) => {
      _data.append(key, data[key]);
    });
  }
  if (loading) {
    this[loading] = true;
  }
  return new Promise((resolve, reject) => {
    instance({
      url,
      method,
      data: _data,
      params,
    })
      .then((res) => {
        if (res.status >= 200 && res.status < 300) {
          if (res.data.status === 0) {
            resolve(res.data);
          } else if (res.data.status === 10) {
            Message.error(res.data.msg);
            setTimeout(() => {
              router.push({
                name: "login",
                query: {
                  redirect: router.currentRoute.fullPath,
                },
              });
            }, 1500);
          } else {
            Message.error(res.data.msg);
            reject(res.data.msg);
          }
        } else {
          Message.warning(res.statusText);
          reject(res.statusText);
        }
      })
      .catch((err) => {
        Message.error(err);
        reject(err);
      })
      .finally(() => {
        this[loading] = false;
      });
  });
};

export default http;
