import axios from "axios";
import router from "@/router";
import { Message, Loading } from "element-ui";
// 添加请求拦截器
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded";
axios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // config.baseURL = "";
    // if(config.url = 'login.do'){
    //     return config;
    // }
    // if (localStorage.get("token")) {
    //   config.headers.token = localStorage.get("token");
    //   return config;
    // }
    // router.push({
    //     name: 'login',
    //     query:{
    //         redirect: router.currentRoute.fullPath
    //     }
    // })
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
axios.interceptors.response.use(
  function (res) {
    // 对响应数据做点什么
    if (res.status >= 200 && res.status < 300) {
      if (res.data.status === 0) {
        return Promise.resolve(res.data);
      } else if (res.data.status === 10) {
        Message.error(res.data.msg);
        setTimeout(() => {
          router.push({
            name: "login",
            query: {
              redirect: router.currentRoute.fullPath,
            },
          });
        }, 1500);
      } else {
        Message.error(res.data.msg);
        return Promise.reject(res.data.msg);
      }
    } else {
      Message.warning(res.statusText);
      return Promise.reject(res.statusText);
    }
    return res;
  },
  function (error) {
    // 对响应错误做点什么
    Message.error(error);
    return Promise.reject(error);
  }
);
function http(url, method = "GET", data = {}, params = {}, loading) {
  let _data = {},
    _loading;
  if (
    Object.prototype.toString.call(data) === "[object Object]" &&
    Object.keys(data).length > 0
  ) {
    _data = new URLSearchParams();
    Object.keys(data).map((key) => {
      _data.append(key, data[key]);
    });
  }
  if (loading) {
    this[loading] = true;
  } else {
    _loading = Loading.service({
      lock: true,
      text: "Loading",
      spinner: "el-icon-loading",
      background: "rgba(0, 0, 0, 0.7)",
    });
  }
  return axios({
    url,
    method,
    data: _data,
    params,
    loading,
  }).finally(() => {
    loading && (this[loading] = false);
    _loading && _loading.close();
  });
}
// function get(url,params){
//     return axios.get()
// }
export default http;
