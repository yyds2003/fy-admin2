import { Message } from "element-ui";
import { post, get, getTwo } from "./index";
export function login(data) {
  return post("/api/manage/user/login.do", data).then((res) => {
    Message.success(res.msg);
    return res.data;
  });
}

export async function getIndex() {
  const res = await get("/api/manage/statistic/base_count.do");
  return res.data;
}

export async function getSales() {
  const res = await getTwo("/sales");
  return res.data;
}

//获取商品列表
export async function getGoodsList(params) {
  const res = await get("/api/manage/product/list.do", params);
  return res.data;
}

//商品搜索

export async function searchGoodsList(params) {
  const res = await get("/api/manage/product/search.do", params);
  return res.data;
}

//订单搜索

export async function searchOrderList(orderNo) {
  const res = await get("/api/manage/order/search.do", { orderNo });
  return res.data;
}
// 设置商品上下架状态
// POST /manage/product/set_sale_status.do
// 接口ID：42686632
export async function handleStatus(data) {
  const res = await post("/api/manage/product/set_sale_status.do", data);
  Message.success(res.data);
  return res;
}
//获取一级分类
export async function getCategory(categoryId) {
  const res = await get("/api/manage/category/get_category.do", { categoryId });
  return res.data;
}
//新增商品
export async function handleSave(data) {
  const res = await post("/api/manage/product/save.do", data);
  Message.success(res.data);
  return res;
}

//获取商品详情
export async function getGoodsDetail(productId) {
  const res = await get("/api/manage/product/detail.do", { productId });
  return res.data;
}

//获取分类
export async function getGoodsFenlei(params) {
  const res = await get("/api/manage/category/get_category.do", params);
  return res.data;
}

//获取订单
export async function getGoodsDingdan(params) {
  const res = await get("/api/manage/order/list.do", params);
  return res.data;
}

//获取用户
export async function getGoodsUser(params) {
  const res = await get("/api/manage/user/list.do", params);
  return res.data;
}
