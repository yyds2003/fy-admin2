import Vue from "vue";
import {
  Button,
  Tag,
  Form,
  FormItem,
  Input,
  Message,
  Card,
  Container,
  Aside,
  Main,
  Header,
  Menu,
  MenuItem,
  MenuItemGroup,
  Submenu,
  Tooltip,
  Breadcrumb,
  BreadcrumbItem,
  Table,
  TableColumn,
  Pagination,
  Loading,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DatePicker,
  TimePicker,
  Col,
  Dialog,
  Select,
  Option,
  InputNumber,
  Switch,
  Upload,
  Drawer,
  Descriptions,
  DescriptionsItem,
} from "element-ui";
Vue.use(Button);
Vue.use(Tag);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
Vue.use(Card);
Vue.use(Container);
Vue.use(Aside);
Vue.use(Main);
Vue.use(Header);
Vue.use(Menu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Submenu);
Vue.use(Tooltip);
Vue.use(Breadcrumb);
Vue.use(BreadcrumbItem);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Pagination);
Vue.use(Loading.directive);
Vue.use(Dropdown);
Vue.use(DropdownItem);
Vue.use(DropdownMenu);
Vue.use(DatePicker);
Vue.use(TimePicker);
Vue.use(Col);
Vue.use(Dialog);
Vue.use(Select);
Vue.use(Option);
Vue.use(InputNumber);
Vue.use(Switch);
Vue.use(Upload);
Vue.use(Drawer);
Vue.use(Descriptions);
Vue.use(DescriptionsItem);
Vue.prototype.$loading = Loading.service;
Vue.prototype.$message = Message;
Vue.prototype.$ELEMENT = { size: "medium" };
