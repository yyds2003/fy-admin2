import Vue from "vue";
import VueRouter from "vue-router";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
Vue.use(VueRouter);
const routes = [
  {
    path: "/login",
    name: "login",
    component: () => import(/* webpackChunkName: "Login" */ "@/views/Login"),
  },
  {
    path: "/upload",
    name: "upload",
    component: () => import("@/components/Upload.vue"),
  },
  {
    path: "/",
    name: "home",
    component: () => import(/* webpackChunkName: "Home" */ "@/views/Home"),
    meta: {
      title: "首页",
    },
    children: [
      {
        path: "",
        alias: "/index",
        name: "index",
        meta: {
          title: "home",
          closable: false,
        },
        component: () =>
          import(/* webpackChunkName: "Home" */ "@/views/Home/Home.vue"),
      },
      {
        path: "goods",
        name: "goods",
        meta: {
          title: "商品",
          disabled: true,
          key(route) {
            return route.fullPath;
          },
        },
        component: () =>
          import(/* webpackChunkName: "Goods" */ "@/views/Goods"),
        children: [
          {
            path: "",
            alias: "/goods/list",
            name: "goodsList",
            meta: {
              title: "商品列表",
            },
            component: () =>
              import(/* webpackChunkName: "Goods" */ "@/views/Goods/List.vue"),
          },
          {
            path: "add",
            name: "goodsAdd",
            meta: {
              title: "新增商品",
            },
            component: () =>
              import(
                /* webpackChunkName: "Goods" */ "@/views/Goods/HandleGoods.vue"
              ),
          },
          ,
          {
            path: "edit/:id",
            name: "goodsEdit",
            meta: {
              title: "编辑商品",
            },
            props: true,
            component: () =>
              import(
                /* webpackChunkName: "Goods" */ "@/views/Goods/HandleGoods.vue"
              ),
          },
          {
            path: "fenlei",
            alias: "/goods/fenlei",
            name: "goodsfenlei",
            meta: {
              title: "品类管理",
            },
            component: () =>
              import(/* webpackChunkName: "Goods" */ "@/views/Goods/fenlei.vue"),
          },
          {
            path: "dingdan",
            alias: "/goods/dingdan",
            name: "goodsdingdan",
            meta: {
              title: "订单管理",
            },
            component: () =>
              import(/* webpackChunkName: "Goods" */ "@/views/Goods/dingdan.vue"),
          },
          {
            path: "user",
            alias: "/goods/user",
            name: "goodsuser",
            meta: {
              title: "用户管理",
            },
            component: () =>
              import(/* webpackChunkName: "Goods" */ "@/views/Goods/user.vue"),
          },
        ],
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  NProgress.start();
  if (localStorage.getItem("username")) {
    return next();
  }
  if (to.name != "login")
    return next({ name: "login", query: { redirect: from.fullPath } });
  next();
});
router.afterEach((to, from) => {
  NProgress.done();
});
router.onError(() => {
  NProgress.done();
});
//登录拦截守卫
// router.beforeEach((to, from, next) => {
//   if (to.name !== "login") {
//     if()
//     console.log("*****", document.cookie);
//     next();
//   } else {
//     next();
//   }
// });

export default router;
