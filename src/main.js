import Vue from "vue";
import "normalize.css";
import RouterTab from "vue-router-tab";
import "vue-router-tab/dist/lib/vue-router-tab.css";
//全屏插件
import VueFullscreen from "vue-fullscreen";
// import axios from "axios";
// import http from "@/http";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/plugins/elementui";
// import "@/themes/element-variables.scss";
import "@/plugins/gt";
// import "@/themes/var.scss";
Vue.config.productionTip = false;
Vue.use(RouterTab);
Vue.use(VueFullscreen);
// axios.defaults.headers.post["Content-Type"] =
//   "application/x-www-form-urlencoded";
// axios.defaults.headers.token = localStorage.getItem("token") || "";
// axios.defaults.headers.post["Content-Type"] = "application/form-data";
// Vue.prototype.$http = http;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
